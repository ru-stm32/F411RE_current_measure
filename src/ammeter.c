#include "ammeter.h"

#include "gpio.h"
#include "rcc.h"

static volatile bool _data_rdy = false;
static volatile uint32_t _current = 0;
static volatile uint32_t _time = 0;
static volatile uint32_t _threshold = 0;

void ammeter_init(ammeter_t *am) {
	// Timer 2 init
	rcc_APB1ENR_set(RCC_APB1ENR_TIM2EN);
	//TODO uzależnić to od zegara, dodać bibliotekę rcc z funkcją zwracania zegara
	TIM2->PSC = 8000 - 1;
	TIM2->ARR = am->interval - 1;
	TIM2->EGR = TIM_EGR_UG;
	TIM2->CR2 = TIM_CR2_MMS_1;

	// ADC1 init
	rcc_APB2ENR_set(RCC_APB2ENR_ADC1EN);	//adc1 clock
	rcc_AHB1ENR_set(RCC_AHB1ENR_GPIOAEN);	//port a clock

	gpio_pin_cfg(GPIOA, P4, gpio_mode_analog);	//PA4 - ADC1_4

	ADC1->SQR3 = (4 << 0);			// chanel 4
	ADC1->CR1 |= ADC_CR1_EOCIE;		// interrupts enabled
	ADC1->CR2 |= ADC_CR2_EXTEN_0;	// external triger on rising edge
	ADC1->CR2 |= ADC_CR2_EXTSEL_1 | ADC_CR2_EXTSEL_2; // timer 2 trgo event

	NVIC_EnableIRQ(ADC_IRQn);

	ADC1->CR2 |= ADC_CR2_ADON;	//enable adc
	TIM2->CR1 |= TIM_CR1_CEN;	//start timet 2

	am->load_time = 0;

	_threshold = am->current_threshold;
}

bool ammeter_measure_rdy(ammeter_t *am) {
	if (!_data_rdy)
		return false;

	am->current = _current;
	_data_rdy = false;

	am->load_time+=(_time * am->interval) / 1000; //TODO policzyć to jeszcze z interwału
	_time = 0;

	return true;
}

void ADC_IRQHandler(void) {
	if (ADC1->SR & ADC_SR_EOC) {

		_data_rdy = true;
		_current = ADC1->DR;

		if(_current > _threshold) _time++;

		ADC1->SR &= ~ADC_SR_EOC; // clear flag
	}
}
