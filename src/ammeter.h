#ifndef AMMETER_H_
#define AMMETER_H_

#include "stm32f4xx.h"

#include "stdbool.h"

typedef struct {
	uint32_t interval;		// in ms
	uint32_t current;		// in mA
	uint32_t current_threshold;	// in mA
	uint32_t load_time;	// loading time
} ammeter_t;

void ammeter_init(ammeter_t *am);
bool ammeter_measure_rdy(ammeter_t *am);

#endif /* AMMETER_H_ */
