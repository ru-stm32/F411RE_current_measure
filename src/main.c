#include "stm32f4xx.h"

#include "gpio.h"
#include "usart.h"
#include "rcc.h"

#include "ammeter.h"
#include "SSD1306_driver.h"
#include "bmp_source.h"

int main(void) {
	usartInstance_t stLinkCOM;
	ammeter_t measurment;

	// USART INIT
	rcc_APB1ENR_set(RCC_APB1ENR_USART2EN);
	rcc_AHB1ENR_set(RCC_AHB1ENR_GPIOAEN);

	gpio_pin_cfg(GPIOA, P2, gpio_mode_AF7_PP_MS);	//PA2 - USART2 TX
	gpio_pin_cfg(GPIOA, P3, gpio_mode_AF7_PP_MS);	//PA3 - USART2 RX

	stLinkCOM.USARTx = USART2;
	stLinkCOM.baud = 115200;
	usart_init(&stLinkCOM);

	usart_printf(&stLinkCOM, "current measure::console\r\n");

	//olde init
	ssd1306_init();
	ssd1306_clear();

	//	ammeter init
	measurment.interval = 1000;
	measurment.current_threshold = 500;
	ammeter_init(&measurment);

	while (1) {

		if (ammeter_measure_rdy(&measurment)) {
			usart_printf(&stLinkCOM, "%u mA \r\n", measurment.current);
			ssd1306_printf(7,1,"I = %4u mA", measurment.current);
			ssd1306_printf(7, 2,"t = %4u s", measurment.load_time);
		}

	}
}
